#!/usr/bin/env python3

# This file is part of CERN LCG software stack.

# Authors: Grigory Latyshev, Ivan Razumov
# Institute for High Energy Physics, Russia

# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

from __future__ import print_function

import sys
import os
import re
import argparse
import glob
import fnmatch


class EnvProducer(object):

    def __init__(self, lcgdir, gccdir):
        self.lcgdir = lcgdir
        self.gccdir = gccdir
        self.info = {}
        self.sourcedpackages = set()
        self.shell = None
        self.usedev = False
        self.nogcc = False
        self.ignore = []
        self.platform = None
        self.shell_cmd = "bash"
        self.shell_eq = "="
        self.cmd = []
        self.use_binutils = False

        if not self.nogcc:
            release = re.search('LCG_([0-9]{2,})[a-z]?', lcgdir)
            if release:
                try:
                    release = int(release.group(1))
                except ValueError:
                    print("# Warning: could not determine release, will use non-binutils GCC version")
                    release = 0

            if (release is not None) and (91 <= release):
                self.use_binutils = True

    @classmethod
    def readbuildinfo(cls, f):
        basename = os.path.basename(f)
        if basename in ('.buildinfo_gcc', '.buildinfo_binutils', '.buildinfo_clang'):
            return None
        
        res = {"HOME": os.path.dirname(f), "PLATFORM": os.path.basename(os.path.dirname(f))}
        line = open(f, 'r').readlines()[0].strip()
        for par in line.split(', '):
            key, value = par.split(':', 1)
            res.update({key: value.strip()})
        if ',' in res['VERSION']:
            deps = res['VERSION'].split(',')[1:-1]
            res['VERSION'] = res['VERSION'].split(',')[0]
            res['DEPENDENCIES'] = deps
        if 'DEPENDS' in res:
            deps = res['DEPENDS'].split(',')
            res['DEPENDS'] = deps
        return res

    def updateInfo(self, dictionary):
        self.info.update({"{0}-{1}".format(dictionary['NAME'], dictionary['HASH']): dictionary})

    def autoModify(self):
        if self.shell is None:
            self.shell = os.path.basename(getShellName())
        self.shell = 'csh' if 'csh' in self.shell else 'bash'  # unify, bash is default
        self.shell_cmd = 'setenv' if self.shell == 'csh' else 'export'
        self.shell_eq = ' ' if self.shell == 'csh' else '='
        # add virtual 'all' package
        if 'all-virtual' not in self.info:
            allpkg = {"HOME": os.path.dirname(os.path.abspath(sys.argv[0])), "NAME": "all", "HASH": "virtual",
                      "VERSION": "virtual",
                      "PLATFORM": self.platform, "DEPENDENCIES": []}
            for i in set([x for x in self.info if "MCGenerators" not in self.info[x]['HOME']]):
                allpkg["DEPENDENCIES"].append(i)
            self.updateInfo(allpkg)
            if 'all' in self.ignore:
                del self.ignore[self.ignore.index('all')]
                for d in allpkg["DEPENDENCIES"]:
                    self.ignore.append(self.info[d]["NAME"])
        # add virtual 'Grid' package
        if 'Grid-virtual' not in self.info:
            Grid = {"HOME": os.path.dirname(os.path.abspath(sys.argv[0])), "NAME": "Grid", "HASH": "virtual",
                    "VERSION": "virtual",
                    "PLATFORM": self.platform, "DEPENDENCIES": []}
            for i in set([x for x in self.info if os.path.join(self.lcgdir, 'Grid') in self.info[x]['HOME']]):
                Grid["DEPENDENCIES"].append(i)
            self.updateInfo(Grid)
            if 'Grid' in self.ignore:
                del self.ignore[self.ignore.index('Grid')]
                for d in Grid["DEPENDENCIES"]:
                    self.ignore.append(self.info[d]["NAME"])
        self.sourcedpackages = self.sourcedpackages.union(set([self.find_package_by_name(x) for x in self.ignore]))

    def selfcheck(self):
        r = set()
        for i in self.info.keys():
            r.add(self.info[i]['PLATFORM'])
        if len(r) != 1:
            raise RuntimeError("Bug in lcgenv -- mixture of platforms detected")
        if list(r)[0] != self.platform:
            raise RuntimeError("Bug in lcgenv -- envtironment produced for incorrect platform")

    def get_environment(self, package_name, **kwargs):
        return "\n".join(self.get_cmd(package_name, **kwargs))

    def get_cmd(self, package_name, **kwargs):
        def append(instr):
            if instr.__class__ == str:
                tmplist = [x.strip() for x in instr.split('\n') if x.strip() != ""]
            elif instr.__class__ == list:
                tmplist = instr[:]
            for s in tmplist:
                if s not in self.cmd:
                    self.cmd.append(s)

        if 'version' not in kwargs or kwargs['version'] is None:
            name = self.info[package_name]['NAME']
            version = self.info[package_name]['VERSION']
            return self.get_cmd(name, version=version)

        self.autoModify()
        self.selfcheck()
        version = kwargs['version']
        if not self.platform:
            raise RuntimeError("Platform not defined")
        arch = '64' if ('x86_64' in self.platform or 'aarch64' in self.platform) else '32'
        usecsh = 'csh' in self.shell
        shell_cmd = self.shell_cmd
        shell_eq = self.shell_eq
        usedev = self.usedev
        phash = [x for x in self.info
                 if self.info[x]["VERSION"] == version and self.info[x]['NAME'] == package_name]
        if not phash:
            raise KeyError()

        package = self.info[phash[0]]
        name = package['NAME']
        home = package['HOME']
        self.sourcedpackages.add(phash[0])
        if name[0].isdigit():
            name = 'X' + name

        if not self.nogcc and 'gcc' not in self.sourcedpackages:
            this_compiler = self.find_compiler()
            if this_compiler:
                append("source " + this_compiler + ";")
            self.sourcedpackages.add('gcc')
        if "DESTINATION" in package:
            destname = package["DESTINATION"]
        else:
            destname = name
        if name in self.ignore or destname in self.ignore:
            return []
        if os.path.exists(os.path.join(home, 'bin')):
            append("{1} PATH{2}\"{0}:$PATH\";".format(os.path.join(home, 'bin'), shell_cmd, shell_eq))

        for libarch in ['lib' + x for x in ("", arch)]:
            if os.path.exists(os.path.join(home, libarch)):
                if usecsh:
                    append("test $?LD_LIBRARY_PATH -eq 0 && {0} LD_LIBRARY_PATH{1}\"\";".format(shell_cmd, shell_eq))
                append("{1} LD_LIBRARY_PATH{2}\"{0}:$LD_LIBRARY_PATH\";".format(os.path.join(home, libarch), shell_cmd,
                                                                                shell_eq))
                for subdir in os.listdir(os.path.join(home, libarch)):
                    if os.path.isdir(os.path.join(home, libarch, subdir)):
                        if subdir.startswith("python"):
                            if usecsh:
                                append("test $?PYTHONPATH -eq 0 && {0} PYTHONPATH{1}\"\";".format(shell_cmd, shell_eq))
                            append("{1} PYTHONPATH{2}\"{0}:$PYTHONPATH\";".format(
                                os.path.join(home, libarch, subdir, 'site-packages'), shell_cmd, shell_eq))
                        elif subdir.startswith("pkgconfig"):
                            if usecsh:
                                append("test $?PKG_CONFIG_PATH -eq 0 && {0} PKG_CONFIG_PATH{1}\"\";".format(shell_cmd,
                                                                                                            shell_eq))
                            append("{1} PKG_CONFIG_PATH{2}\"{0}:$PKG_CONFIG_PATH\";".format(
                                os.path.join(home, libarch, subdir), shell_cmd, shell_eq))
                        else:
                            append("{1} LD_LIBRARY_PATH{2}\"{0}:$LD_LIBRARY_PATH\";".format(
                                os.path.join(home, libarch, subdir), shell_cmd, shell_eq))
            if usecsh:
                append("test $?CMAKE_PREFIX_PATH -eq 0 && {0} CMAKE_PREFIX_PATH{1}\"\";".format(shell_cmd, shell_eq))
            append("{0} CMAKE_PREFIX_PATH{2}\"{1}:$CMAKE_PREFIX_PATH\";".format(shell_cmd, home, shell_eq))
        if usedev:
            append("{0} LIBRARY_PATH{1}$LD_LIBRARY_PATH;".format(shell_cmd, shell_eq))

        # dependencies
        if 'DEPENDENCIES' in package:
            for dep in package['DEPENDENCIES']:
                print('# found package', dep)
                if dep in self.sourcedpackages:
                    continue
                dep_name = dep.split('-')[0]
                dep_hash = dep.split('-')[-1]
                dep_version = self.find_version(dep_name, dep_hash)
                try:
                    append(self.get_cmd(dep_name, version=dep_version))
                    self.sourcedpackages.add(dep)
                except Exception:
                    pass
        elif 'DEPENDS' in package:
            for dep in package['DEPENDS']:
                dep_name = dep.split('-')[0]
                dep_hash = dep.split('-')[-1]
                if '{0}-{1}'.format(dep_name, dep_hash) in self.sourcedpackages:
                    continue
                dep_version = self.find_version(dep_name, dep_hash)
                try:
                    append(self.get_cmd(dep_name, version=dep_version))
                    self.sourcedpackages.add(dep)
                except KeyError:
                    pass
        # firstly check package directory
        env_filename = None
        if os.path.exists(os.path.join(os.path.dirname(__file__), "env", name)):
            env_filename = os.path.join(os.path.dirname(__file__), "env", name)
        elif os.path.exists(os.path.join(home, ".env")):
            env_filename = os.path.join(home, ".env")

        append("{2} {0}__HOME{3}\"{1}\";".format(name.replace("-", '_').replace('+', 'p').upper(), home, shell_cmd,
                                                shell_eq))
        if env_filename:
            append("cd \"{0}\"".format(home))
            text = open(env_filename, 'r').read()
            if self.shell == "csh":
                text = text.replace('.sh', '.csh')
                text = text.replace('export', 'setenv')
                text = text.replace('=', ' ')
            append(text)
            if self.shell == "csh":
                append("cd - # from {0}".format(home))
            else:
                append("cd - 1>/dev/null # from {0}".format(home))
        return self.cmd

    def get_packages_names(self):
        self.autoModify()
        return [x['NAME'] for x in self.info.values()]

    def get_package_versions(self, packagename):
        self.autoModify()
        try:
            return [self.info[x]['VERSION'] for x in self.info if self.info[x]['NAME'] == packagename]
        except KeyError:
            return []

    def find_package_by_name(self, pkg):
        if pkg in self.info:
            return pkg
        for i in self.info.keys():
            r = re.findall("{0}-.+".format(pkg), i, flags=re.IGNORECASE)
            if r:
                return r[0]
        return None

    def find_version(self, package_name, package_hash):
        self.autoModify()

        try:
            return [self.info[x]['VERSION'] for x in self.info if self.info[x]['NAME'] == package_name and
                    self.info[x]['HASH'] == package_hash][0]
        except Exception:
            return None

    def find_compiler_nocontrib(self):
        try:
            gcc_ver = self.platform.split('gcc')[1].split('-')[0]
            gcc_spec = self.platform.split('-gcc')[0]
            gcc_dot_ver = gcc_ver[0] + '.' + gcc_ver[1]
            gcc_root = os.path.join(glob.glob(os.path.join(self.lcgdir, 'gcc', gcc_dot_ver + '*'))[-1], gcc_spec)
            setup_file = 'setup.csh' if 'csh' in self.shell else 'setup.sh'
            if os.path.exists(os.path.join(gcc_root, setup_file)):
                return os.path.join(gcc_root, setup_file)
        except IndexError:
            return None

    def find_compiler(self):
        try:
            line = open(os.path.join(self.lcgdir, 'LCG_contrib_' + self.platform + '.txt')).read().strip()
            gcc_version = line.split(';')[2].strip()
            gcc_relpath = line.split(';')[3].strip()
            if (gcc_version.startswith('6.2') or gcc_version.startswith('7')) and self.use_binutils:
                print("# Using gcc with newer binutils")
                gcc_relpath_parts = gcc_relpath.split('/')
                gcc_relpath = "{0}/{1}binutils/{2}".format(*gcc_relpath_parts)

            gcc_root = os.path.join(self.lcgdir, gcc_relpath)
            if os.path.exists(gcc_root):
                print("# Using gcc from release")
            else:
                print("# Using globaly provided gcc from", self.gccdir)
                gcc_root = os.path.join(self.gccdir, gcc_relpath)

            setup_file = 'setup.csh' if 'csh' in self.shell else 'setup.sh'

            if os.path.exists(os.path.join(gcc_root, setup_file)):
                return os.path.join(gcc_root, setup_file)
            print("# gcc not found :(")
            return self.find_compiler_nocontrib()
        except IndexError:
            print("# Error reading contrib file")
            return self.find_compiler_nocontrib()
        except IOError:
            print("# No LCG_contrib found!")
            return self.find_compiler_nocontrib()


def getShellName():
    try:
        return os.readlink('/proc/{0}/exe'.format(os.getppid()))
    except (IOError, OSError):
        return '/bin/bash'


def get_filenames(root, platform=None, maxdepth=4):
    init_depth = len(root.split(os.path.sep))
    for (dirpath, dirnames, filenames) in os.walk(root, followlinks=True):
        if len(dirpath.split(os.path.sep)) - init_depth > maxdepth:
            dirnames[:] = []
        for p in platform:
            if p in dirnames:
                d = os.path.join(os.path.abspath(dirpath), p)
                if fnmatch.filter(os.listdir(d), '.buildinfo*'):
                    yield (d, [x for x in os.listdir(d) if os.path.isfile(os.path.join(d, x))])
                    dirnames[:] = []
                    continue
        yield (os.path.abspath(dirpath), filenames)


def main():
    helpstring = """{0} [options] [arguments]

This package provides environment for packages in LCG software stack.
Available combinations of positional arguments are:
  {0} <platform>
  {0} <platform> <package>
  {0} <platform> <package> <version>
  {0} <package>  <version> <platform>

Only two last variants produce environment. To setup environment in current shell:
  eval "`{0} <platform> <package> <version>`"
  eval "`{0} <package>  <version> <platform>`"
"""
    parser = argparse.ArgumentParser(usage=helpstring.format(sys.argv[0]))
    parser.add_argument('arguments', metavar='...', nargs='*', help=argparse.SUPPRESS)
    parser.add_argument('-s', '--shell', help="prepare environment for *csh* or *bash* shell", action="store",
                        default=None, dest='shell')
    parser.add_argument('-d', '--developer', help="prepare environment for developers", action="store_true",
                        default=False, dest='usedev')
    parser.add_argument('-i', '--ignore', help="ignore package and all its dependencies", action="append", default=[],
                        dest='ignore', metavar="PACKAGE")
    parser.add_argument('-p', '--lcgpath', help="top directory of LCG release", action="store",
                        default='.', dest='lcgpath')
    parser.add_argument('-T', '--no-txt', help="ignore .txt files if they exist", action="store_true",
                        default=False, dest='notxt')
    parser.add_argument('-G', '--no-gcc', help="skip gcc enviroment setup ", action="store_true", default=False,
                        dest='nogcc')
    parser.add_argument('-g', '--gccpath', help="directory containing all gcc version", action="store",
                        default="/cvmfs/sft.cern.ch/lcg/contrib", dest='gccpath')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s 1.3.3')
    args = parser.parse_args()
    arg = [] if args.arguments is None else args.arguments
    if 'LCGENV_PATH' in os.environ:
        lcgpath = os.path.abspath(os.path.join(os.environ['LCGENV_PATH'], args.lcgpath))
    else:
        lcgpath = os.path.abspath(args.lcgpath)
    if not os.path.isdir(lcgpath):
        print("# '{0}' directory doesn't exist. Exiting".format(lcgpath))
        return 1

    P = EnvProducer(lcgpath, args.gccpath)
    P.shell = args.shell
    P.usedev = args.usedev
    P.ignore = args.ignore
    P.nogcc = args.nogcc
    platforms = set()

    this_platform = None
    this_package = None
    if len(arg) == 2 or len(arg) == 1:
        this_platform = arg[0]
        if len(arg) == 2:
            this_package = arg[1]

    txtfiles = glob.glob(os.path.join(lcgpath, 'LCG*.txt')) if not args.notxt else []
    notxt = args.notxt or (len(txtfiles) < 2)

    # Determine possible platforms
    if not notxt:  # Each platform needs 2 files: externals and generators
        for txt in txtfiles:
            try:
                platforms.add(os.path.basename(txt).split('_', 2)[2].replace('.txt', ''))
            except IndexError:
                pass
    else:  # Since we don't have txt files, we have to traverse the folders in order to find all the platforms
        if len(arg) == 3:
            plat = [arg[0], arg[2]]
        else:
            plat = [this_platform]
        walker = get_filenames(lcgpath, plat)
        configfiles = []
        for (dirpath, filenames) in walker:
            configfiles.extend([os.path.join(dirpath, x) for x in fnmatch.filter(filenames, '.buildinfo*')])
        for f in configfiles:
            platform_test = os.path.basename(os.path.dirname(f))
            r = P.readbuildinfo(f)
            if r is None:
                continue
                
            if len(arg) == 3:
                if arg[0] == platform_test or arg[2] == platform_test:
                    P.updateInfo(r)
            else:
                if this_platform == platform_test:
                    P.updateInfo(r)
            platforms.add(r['PLATFORM'])

    if not arg:
        print("# Available platforms in '{0}' are:".format(P.lcgdir))
        for pl in platforms:
            print("#  ", pl)
        return 2

    # After this line len(arg) is at least 1
    this_version = None

    # Handle all kind of arguments
    if len(arg) == 3:
        if arg[0] in platforms:
            this_platform = arg[0]
            this_package = arg[1]
            this_version = arg[2]
        elif arg[2] in platforms:
            this_platform = arg[2]
            this_package = arg[0]
            this_version = arg[1]
        else:
            print("# No platform '{0}' found".format(this_platform))
            print("# Available platforms in '{0}' are:".format(P.lcgdir))
            print("\n".join(["#   " + x for x in platforms]))
            return 1

    if this_platform not in platforms:
        print("# No platform '{0}' found".format(this_platform))
        print("# Available platforms in '{0}' are:".format(P.lcgdir))
        print("\n".join(["#   " + x for x in platforms]))
        return 1
    P.platform = this_platform

    if not notxt:
        txtfiles = glob.glob(os.path.join(lcgpath, '*%s*.txt' % this_platform))
        for txt in txtfiles:
            for line in open(txt, 'r').readlines():
                try:
                    name, pkghash, version, home, deps = [x.strip() for x in line.split(';')]
                except ValueError:
                    continue
                else:
                    r = {'NAME': name, 'HASH': pkghash, 'HOME': os.path.abspath(os.path.join(lcgpath, home)),
                         'PLATFORM': this_platform,
                         'DEPENDENCIES': [x for x in deps.split(',') if x != '' and x != name + '-' + pkghash],
                         'VERSION': version}
                    P.info.update({"{0}-{1}".format(r['NAME'], r['HASH']): r})

    if len(arg) == 1:
        # List all packages for platform arg[0]
        print("# Available packages for platform", this_platform)
        for p in sorted(set(P.get_packages_names())):
            print("#  ", p)

        return 2

    if this_package == 'qt5':
        this_package = 'Qt5'
                
    # After this line len(arg) is at least 2
    if this_package not in P.get_packages_names():
        print("# No package '{0}' found.".format(this_package))
        testset = set([x for x in P.get_packages_names() if this_package.lower() in x.lower()])
        if testset:
            print("# Did you mean one of these?")
            for i in sorted(testset):
                print("#  ", i)
        return 2
    if len(P.get_package_versions(this_package)) == 1 and this_package != 'herwig++':
        this_version = P.get_package_versions(this_package)[0]
        print(P.get_environment(this_package, version=this_version))
        return 0
    if len(arg) == 2:
        print("# Available versions: ")
        for v in sorted(set(P.get_package_versions(this_package))):
            print("#  ", v)
        return 2

    # After this line len(arg) is at least 3
    if this_package == 'herwig++' and int(this_version.split('.', 1)[0]) > 2:
        this_package = 'herwig3'

    if this_version not in P.get_package_versions(this_package):
        print("# No version '{0}' found for package '{1}'".format(this_version, this_package))
        print("# Available versions for package '{0}' are:".format(this_package))
        print("\n".join(["#   " + x for x in sorted(set(P.get_package_versions(this_package)))]))
        return 1

    print(P.get_environment(this_package, version=this_version))


if __name__ == '__main__':
    ec = main()
    sys.exit(ec)
