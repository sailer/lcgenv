lcgenv
======

This package provides environment for packages in SFT LCG releases.

Usage
-----
    eval "`lcgenv [options] <platform> <package> [<version>]`"
    eval "`lcgenv [options] <package>  <version> <platform>`" 

LCGENV_PATH environment variable can be used as path to releases area:

    export LCGENV_PATH=/cvmfs/sft.cern.ch/lcg/releases
    lcgenv -p LCG_79 x86_64-slc6-gcc48-opt ROOT
        

Arguments
---------
    lcgenv [options]                                  # print available platforms
    lcgenv [options] <platform>                       # print available packages for <platform>
    lcgenv [options] <platform> <package>             # print available versions for <package> or print environement (if there is only one version) for <package>
    lcgenv [options] <platform> <package> <version>   # print environment for <package>
    lcgenv [options] <package>  <version> <platform>  # print environment for <package>

Options
------
    -s SHELL, --shell SHELL            prepare environment for *csh* or *bash* shell
    -d, --developer                    prepare environment for developers
    -i PACKAGE, --ignore PACKAGE       ignore package and its dependencies
    -p LCGPATH, --lcgpath LCGPATH      top directory of LCG release
    -T, --no-txt                       ignore .txt files if they exist
    -G, --no-gcc                       skip gcc enviroment setup
    -v, --version                      show program's version number and exit

Environment files
------

Despite of `lcgenv` generates environment for LCG package there are some cases when additional package setup is needed.
In this case additional environment should be written to `env/<package name>` file on the top of lcgenv repository.

For each package `lcgenv` sets `<PACKAGE>_HOME` shell variable which points to package's location.
Note that package name in `<PACKAGE>_HOME` must be in **upper** case.

For example `env/Boost`:

```
export CPLUS_INCLUDE_PATH=${BOOST_HOME}/include:$CPLUS_INCLUDE_PATH
export C_INCLUDE_PATH=${BOOST_HOME}/include:$C_INCLUDE_PATH
```

argparse module
------
argparse is (c) 2006-2009 Steven J. Bethard <steven.bethard@gmail.com>.

The argparse module was contributed to Python as of Python 2.7 and thus
was licensed under the Python license. Same license applies to all files in
the argparse package project.

For details about the Python License, please see https://www.python.org/download/releases/2.7/license/ .